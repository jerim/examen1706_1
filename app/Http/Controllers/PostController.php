<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Auth;
use Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $posts= Post::all();
        return view('posts.index',['posts'=>$posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $post=Post::find($id);
         $this->authorize('update', $post);
         return view('posts.show',['post'=>$post]);
    }

    public function storeComment(Request $request,$id){

        $rules=[
        'text' => 'required|min:1|max:500',

         ];
        $messages = [
            'required' => 'Falta por escribir el texto',
            'min'=> 'minimo tiene que tener 1 caracteres',
            'max'=>'no puede tener más de 500 caracteres'
        ];

        $request->validate($rules,$messages);

        $comment = new Comment;
        $comment->text = $request->text;
        $comment->post_id = $id;
        $comment->user_id = \Auth::user()->id;
        $comment->save();

        return back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::find($id);
         $this->authorize('update', $post);
        return view('posts.edit',['post'=>$post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
        'content' => 'required|min:4|max:500',

         ];
        $messages = [
            'required' => 'Falta por escribir el texto',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 500 caracteres'
        ];

        $request->validate($rules,$messages);

        $post = Post::find($id);
        $post->fill($request->all());
        $post->user_id = \Auth::user()->id;
        $post->save();


        return redirect('/posts');
    }

    public function like(Request $request, $id){

        $post=Post::find($id);
        $request->session()->put('post',$post);

        return back();

    }

    public function forget(Request $request){

    $request->session()->forget('post');

        return back();

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
