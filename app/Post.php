<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable =['title','content','date','user_id'];
     public function user(){
        return  $this->belongsTo(User::class);
     }

     public function comments(){

        return $this->hasMany(Comment::class);
     }

     public function getStr() {
        return substr($this->content,100);
    }
}
