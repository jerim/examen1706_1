# Examen de junio/2019 DWS

- Partimos de Laravel 5.8 + instalación de usuarios (artisan make:auth)
- Ojo. La tabla de usuario tiene un atributo 'admin' para clasificar los usuarios en ordinarios y administradores.

## Objetivo y base de datos:

- Realizar un blog:
- Las rutas ya están registradas en web.php. No modificar este fichero.
- Datos:
    - Post (id, title, content, user_id, date)
    - Comment (id, user_id, post_id, text)


## Objetivo y base de datos:

- Parte 1 Obligatoria (4 puntos):
    
	1. Lista de entradas ('/posts'). 
		- La lista de entradas debe  aparecer ordenada por fecha de publicación. 
        - En la lista debe aparecer el título, los 100 primeros 140 caracteres seguidos de unos puntos suspensivos y un enlace a la ruta show ('/show') con el texto "leer más". 

    2.  Show ('/posts/{id}')
        - Muestra título y texto completo
        - Seguidamente autor y fecha.
        - Seguidamente los comentarios: autor + texto

	3. Creación de comentarios. Sólo usuarios logueados incluida validación.
        - Ruta del formulario: ver web.php
        - Rellenar texto, valor requerido y maximo 500 caracteres.
        - La fecha se toma la de hoy.
        - El usuario el que está logueado.



- Parte 2. (1 Punto)
    - La lista de entradas y el show visible para todos.
    - Mediante políticas, el edit sólo puede realizarlo el propietario. Idem ver el botón.
	- Creación de comentarios sólo para usuarios logueados.

- Parte 3. (1 Punto) 
    - Guardar en sesión una lista de post "favoritos".
    - En el show debemos hacer que junto al título aparezca "ME GUSTA" en aquellos posts marcados como favoritos.
