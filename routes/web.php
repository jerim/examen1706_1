<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/posts/{id}/like', 'PostController@like'); //me gusta
Route::post('/posts/{id}/store', 'PostController@storeComment'); //alta comentarios

Route::get('/posts/forget', 'PostController@forget');
Route::resource('/posts', 'PostController');


//shows
Route::get('posts/{id}', function ($id) {
   return "Ver contenido de post $id";
});
