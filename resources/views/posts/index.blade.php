@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">Posts<br>

        </div>


        <div class="card-body" >
          <table class="table">
            @foreach($posts as $post)

            <tr>
              <td><strong>{{$post->title}}</strong></td>
            </tr>
            <tr>
              <td>
                {{$post->content}}
              </td>

              <td>
               <form method="post" action="/posts/{{$post->id}}">
                 {{ csrf_field() }}
                 <a  href="/posts/{{$post->id}}" class="btn btn-success"  role="button" >Leer mas...</a>
                  <a  href="/posts/{{$post->id}}/like" class="btn btn-success"  role="button" >Favoritos</a>
                  @can('update',$post)
                   <a  href="/posts/{{$post->id}}/edit" class="btn btn-success"  role="button" >Editar</a></td>
                  @else

                  <td></td>

                  @endcan
               </form>

             </td>
           </td>
         </tr>

          <tr>
            <td>
               <strong> {{$post->user->name}} -{{$post->date}}</strong>
            </td>
          </tr>

         @endforeach


       </table>

     </div>

  </div>
</div>
</div>
</div>
@endsection
