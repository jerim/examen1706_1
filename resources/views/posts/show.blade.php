@extends('layouts.app')

@section('title', 'Post')

@section('content')

           @if(Session::has('post'))

            <strong style="color:red">ME GUSTA!!!!!!!!</strong>
             <a  href="/posts/forget" class="btn btn-warning"  role="button" >Decir que no me gusta</a>
           @endif

            <h1>
            {{$post->title}}
            </h1>

            <ul>
                <li> {{$post->content}} </li>
                <li>Publicado por <strong> {{$post->user->name }} - {{$post->date}}</strong> </li>

            </ul>

            <h1>
               Comentarios
            </h1>
            <ul>

                 @foreach($post->comments as $po )
                <li> <strong>  {{$post->user->name }}</strong>: {{$po->text}}</li>
                @endforeach
                    </li>
            </ul>


            <ul>
             <form class="form"  method="post" action="/posts/{{$post->id}}/store">
                {{ csrf_field() }}

                <div class="form-group">
                 <label>Nuevo comentario</label>
                 <input class="form-control" type="text" name="text" value="{{old('text')}}">

                 @if ($errors->first('text'))
                 <div class="alert alert-danger ">
                    {{$errors->first('text')}}
                </div>
                @endif

            </div>

            <input type="submit" value="Enviar consulta" class="btn btn"  role="button">

        </form>

</ul>

@endsection

