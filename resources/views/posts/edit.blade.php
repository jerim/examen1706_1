@extends('layouts.app')

@section('title', 'Post')

@section('content')
            <h1>
            {{$post->title}}
            </h1>

            <ul>
             <form class="form"  method="post" action="/posts/{{$post->id}}">
                {{ csrf_field() }}
                  <input type="hidden" name="_method" value="put">

                <div class="form-group">
                <label for="content"></label>
                <textarea id="content" name = "content" rows="10" cols="50" onKeyPress class="form-control">
                {{$post->content}}
                    </textarea>

                 @if ($errors->first('content'))
                 <div class="alert alert-danger ">
                    {{$errors->first('content')}}
                </div>
                @endif

            </div>

            <input type="submit" value="Enviar consulta" class="btn btn"  role="button">

        </form>

</ul>

<ul>

    <li>Publicado por <strong> {{$post->user->name }} - {{$post->date}}</strong> </li>

</ul>
@endsection




